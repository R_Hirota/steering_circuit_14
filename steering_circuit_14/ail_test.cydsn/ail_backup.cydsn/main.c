/* ========================================
 * aileron14 version 1.0 2014/4/19
 * 1...right 2...left
 *
 * 元ファイルです
 * ========================================
*/
#include <project.h>

#define WRITE()		DE_Write(1)
#define	READ()		DE_Write(0)

#define	RS485_PACKET_LENGTH	(6)

typedef union _tagRS485_PACKET {
	struct {
		unsigned char bStatus;
		unsigned char bCheckSum;
		unsigned short wData[2];
	} _pak;
	unsigned char _byte[RS485_PACKET_LENGTH];
} PACKET;

void rs485_Start(void);
void rs485_isr(void);
char rs485_CheckPacket(char, PACKET*);
char CheckValid(PACKET *);
void Servo_Start(void);
void Terminal_Count_ISR(void);
unsigned int FormatValueAil(float);

#define	PACKET_ENDIANCHANGE(x)	{\
	char temp; \
	temp = x->_byte[2]; \
	x->_byte[2] = x->_byte[3]; \
	x->_byte[3] = temp; \
	temp = x->_byte[4]; \
	x->_byte[4] = x->_byte[5]; \
	x->_byte[5] = temp; \
	}

//Global variable here.

unsigned int aileron_buffer;
PACKET pak;

void main(){ 
    
    rs485_Start();   
    Servo_Start();
    CyGlobalIntEnable;  // Uncomment this line to enable global interrupts. 
	while(1) {
		if(rs485_CheckPacket(0x3c, &pak)==0) {
            LED_2_Write(1);
            LED_1_Write(0);
            if(CheckValid(&pak)==1) {
                aileron_buffer = pak._pak.wData[0];
                LED_2_Write(0);
                LED_1_Write(0);
            } else {
                LED_2_Write(1);
                LED_1_Write(1);
            }
        } else {
            LED_2_Write(1);
            LED_1_Write(1);
        } 
	}
    return;
}

void Timer_Set(unsigned int ms){
	Timer_WriteCounter(ms);
	return;
}

void Timer_Delayus(unsigned int us){
	Timer_Set(us + 50);
	while(Timer_ReadCounter() > 50) {}
	return;
}

void rs485_Start(void){
	UART_Start();
	Timer_Start();
	return;
}

/*
 * rs485_CheckPacket
 * 複数アドレス対応版 ビット3-6が一致で応答
 * addr : device address (7bit unsigned)
 * pak : pointer to PACKET structure
 */
char rs485_CheckPacket(
	char addr,
	PACKET *pak)
{
	char rcv;
	unsigned char i;
	
	if((UART_ReadRxStatus() & UART_RX_STS_FIFO_NOTEMPTY) == 0)
	{
        LED_1_Write(1);
		READ();
		return 1;
	}
	LED_1_Write(1);
	rcv = UART_ReadRxData();
	UART_ClearRxBuffer();
	if(addr != (rcv & 0x7f)){
		Timer_Set(200);
		READ();
		while(Timer_ReadCounter() > 49){
			if((UART_ReadRxStatus() & UART_RX_STS_FIFO_NOTEMPTY) != 0){
				Timer_Delayus(900);
				UART_ClearRxBuffer();
				return 1;
			}
		}
		UART_ClearRxBuffer();
		return 1;
	}
	
    LED_1_Write(0);
	WRITE();
	UART_PutChar(addr);
	while(!(UART_ReadTxStatus() & UART_TX_STS_COMPLETE)) {}
	UART_ClearRxBuffer();
	
	if(rcv & 0x80){
		// send data to master
		PACKET_ENDIANCHANGE(pak);
		for(i = 0; i < 6; i++){
			UART_PutChar(pak->_byte[i]);
			while(!(UART_ReadTxStatus() & UART_TX_STS_COMPLETE)) {}
			UART_ClearRxBuffer();
		}
	}
	else{
		// receive data from master
		READ();
		Timer_Set(1010);
		i = 0;
		while((i < 6) && (Timer_ReadCounter() > 10)){
			if(UART_ReadRxStatus() & UART_RX_STS_FIFO_NOTEMPTY){
				pak->_byte[i++] = UART_ReadRxData();
				UART_ClearRxBuffer();
			}
		}
		//PACKET_ENDIANCHANGE(pak);
	}
	WRITE();
	UART_PutChar(addr);
	while(!(UART_ReadTxStatus() & UART_TX_STS_COMPLETE)) {}
	UART_ClearRxBuffer();
	READ();
	return 0;
}

char CheckValid(PACKET *pak){
    unsigned char sum = 0;
    int i;
    
    for(i = 0; i < 4; i++){
        sum += pak->_byte[i+2];    
    }
    if((~sum & 0xff) == pak->_pak.bCheckSum) {
        return 1;
    } else {
        return 0;
    }
}

void Servo_Start(void){
    PWM_1_Start();
    isr_tc_StartEx(Terminal_Count_ISR);
    return;
}

CY_ISR(Terminal_Count_ISR) {
    PWM_1_WriteCompare1(aileron_buffer);
    PWM_1_ReadStatusRegister();
    return;
}

/* [] END OF FILE */

