/*******************************************************************************
* File Name: rs485_isr.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_rs485_isr_H)
#define CY_ISR_rs485_isr_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void rs485_isr_Start(void);
void rs485_isr_StartEx(cyisraddress address);
void rs485_isr_Stop(void);

CY_ISR_PROTO(rs485_isr_Interrupt);

void rs485_isr_SetVector(cyisraddress address);
cyisraddress rs485_isr_GetVector(void);

void rs485_isr_SetPriority(uint8 priority);
uint8 rs485_isr_GetPriority(void);

void rs485_isr_Enable(void);
uint8 rs485_isr_GetState(void);
void rs485_isr_Disable(void);

void rs485_isr_SetPending(void);
void rs485_isr_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the rs485_isr ISR. */
#define rs485_isr_INTC_VECTOR            ((reg32 *) rs485_isr__INTC_VECT)

/* Address of the rs485_isr ISR priority. */
#define rs485_isr_INTC_PRIOR             ((reg8 *) rs485_isr__INTC_PRIOR_REG)

/* Priority of the rs485_isr interrupt. */
#define rs485_isr_INTC_PRIOR_NUMBER      rs485_isr__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable rs485_isr interrupt. */
#define rs485_isr_INTC_SET_EN            ((reg32 *) rs485_isr__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the rs485_isr interrupt. */
#define rs485_isr_INTC_CLR_EN            ((reg32 *) rs485_isr__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the rs485_isr interrupt state to pending. */
#define rs485_isr_INTC_SET_PD            ((reg32 *) rs485_isr__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the rs485_isr interrupt. */
#define rs485_isr_INTC_CLR_PD            ((reg32 *) rs485_isr__INTC_CLR_PD_REG)


#endif /* CY_ISR_rs485_isr_H */


/* [] END OF FILE */
