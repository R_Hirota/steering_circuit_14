/*******************************************************************************
* File Name: ail.c  
* Version 1.90
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "ail.h"

/* APIs are not generated for P15[7:6] on PSoC 5 */
#if !(CY_PSOC5A &&\
	 ail__PORT == 15 && ((ail__MASK & 0xC0) != 0))


/*******************************************************************************
* Function Name: ail_Write
********************************************************************************
*
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  None
*  
*******************************************************************************/
void ail_Write(uint8 value) 
{
    uint8 staticBits = (ail_DR & (uint8)(~ail_MASK));
    ail_DR = staticBits | ((uint8)(value << ail_SHIFT) & ail_MASK);
}


/*******************************************************************************
* Function Name: ail_SetDriveMode
********************************************************************************
*
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  None
*
*******************************************************************************/
void ail_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(ail_0, mode);
}


/*******************************************************************************
* Function Name: ail_Read
********************************************************************************
*
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  None
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro ail_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 ail_Read(void) 
{
    return (ail_PS & ail_MASK) >> ail_SHIFT;
}


/*******************************************************************************
* Function Name: ail_ReadDataReg
********************************************************************************
*
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 ail_ReadDataReg(void) 
{
    return (ail_DR & ail_MASK) >> ail_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(ail_INTSTAT) 

    /*******************************************************************************
    * Function Name: ail_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  None 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 ail_ClearInterrupt(void) 
    {
        return (ail_INTSTAT & ail_MASK) >> ail_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif /* CY_PSOC5A... */

    
/* [] END OF FILE */
