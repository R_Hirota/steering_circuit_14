/* ========================================
 * servo check for aileron
 * 
 *　R.Hirota 2014/4/18
 * 1...aileron right 2...aileron left
 * ========================================
*/
#include <project.h>

uint8 return_val;

int NEUTRAL_R = 1500; //2100
    NEUTRAL_L = 1500; //950
    DIAL = 100;

//#define	READ()		DE_Write(0)

//#define	RS485_PACKET_LENGTH	(6)

/*	struct {
		unsigned char bStatus;
		unsigned char bCheckSum;
		unsigned short wData[2];
	} _pak;
	unsigned char _byte[RS485_PACKET_LENGTH];
} PACKET;

void rs485_Start(void);
void rs485_isr(void);
char rs485_CheckPacket(char, PACKET*);
char CheckValid(PACKET *);
void Servo_Start(void);
void Terminal_Count_ISR(void);

#define	PACKET_ENDIANCHANGE(x)	{\
	char temp; \
	temp = x->_byte[2]; \
	x->_byte[2] = x->_byte[3]; \
	x->_byte[3] = temp; \
	temp = x->_byte[4]; \
	x->_byte[4] = x->_byte[5]; \
	x->_byte[5] = temp; \
	}

//Global variable here.

int aileron_buffer = 1500;
PACKET pak;
*/

#define RIGHT
//#define LEFT

void main()
{
    PWM_1_Start();

    for(;;){
        return_val = Status_Reg_1_Read();
        return_val = ~return_val & 0x0f;
        LED_3_Write(1);
        
        #ifdef LEFT 
            switch(return_val){
                case 0x00:
                    PWM_1_WriteCompare(NEUTRAL_L - DIAL*8);
                    break;
                case 0x01:
                    PWM_1_WriteCompare(NEUTRAL_L - DIAL*7);
                    break;
                case 0x02:
                    PWM_1_WriteCompare(NEUTRAL_L - DIAL*6);
                    break;
                case 0x03:
                    PWM_1_WriteCompare(NEUTRAL_L - DIAL*5);
                    break;
                case 0x04:
                    PWM_1_WriteCompare(NEUTRAL_L - DIAL*4);
                    break;
                case 0x05:
                    PWM_1_WriteCompare(NEUTRAL_L - DIAL*3);
                    break;
                case 0x06:
                    PWM_1_WriteCompare(NEUTRAL_L - DIAL*2);
                    break;
                case 0x07:
                    PWM_1_WriteCompare(NEUTRAL_L - DIAL);
                    break;
                case 0x08:
                    PWM_1_WriteCompare(NEUTRAL_L);
                    break;
                case 0x09:
                    PWM_1_WriteCompare(NEUTRAL_L + DIAL);
                    break;
                case 0x0a:
                    PWM_1_WriteCompare(NEUTRAL_L + DIAL*2);
                    break;
                case 0x0b:
                    PWM_1_WriteCompare(NEUTRAL_L + DIAL*3);
                    break;
                case 0x0c:
                    PWM_1_WriteCompare(NEUTRAL_L + DIAL*4);
                    break;
                case 0x0d:
                    PWM_1_WriteCompare(NEUTRAL_L + DIAL*5);
                    break;
                case 0x0e:
                    PWM_1_WriteCompare(NEUTRAL_L + DIAL*6);
                    break;
                case 0x0f:
                    PWM_1_WriteCompare(NEUTRAL_L + DIAL*7);
                    break;
        #endif
        
        #ifdef RIGHT 
            switch(return_val){
                case 0x0f:
                    PWM_1_WriteCompare(NEUTRAL_R + DIAL*7);
                    break;
                case 0x0e:
                    PWM_1_WriteCompare(NEUTRAL_R + DIAL*6);
                    break;
                case 0x0d:
                    PWM_1_WriteCompare(NEUTRAL_R + DIAL*5);
                    break;
                case 0x0c:
                    PWM_1_WriteCompare(NEUTRAL_R + DIAL*4);
                    break;
                case 0x0b:
                    PWM_1_WriteCompare(NEUTRAL_R + DIAL*3);
                    break;
                case 0x0a:
                    PWM_1_WriteCompare(NEUTRAL_R + DIAL*2);
                    break;
                case 0x09:
                    PWM_1_WriteCompare(NEUTRAL_R + DIAL);
                    break;
                case 0x08:
                    PWM_1_WriteCompare(NEUTRAL_R);
                    break;
                case 0x07:
                    PWM_1_WriteCompare(NEUTRAL_R - DIAL);
                    break;
                case 0x06:
                    PWM_1_WriteCompare(NEUTRAL_R - DIAL*2);
                    break;
                case 0x05:
                    PWM_1_WriteCompare(NEUTRAL_R - DIAL*3);
                    break;
                case 0x04:
                    PWM_1_WriteCompare(NEUTRAL_R - DIAL*4);
                    break;
                case 0x03:
                    PWM_1_WriteCompare(NEUTRAL_R - DIAL*5);
                    break;
                case 0x02:
                    PWM_1_WriteCompare(NEUTRAL_R - DIAL*6);
                    break;
                case 0x01:
                    PWM_1_WriteCompare(NEUTRAL_R - DIAL*7);
                    break;
                case 0x00:
                    PWM_1_WriteCompare(NEUTRAL_R - DIAL*8);
                    break;
            }
            
        #endif
    }
}
            
 /*       int i = 1500; 
        rs485_Start();   
        //Servo_Start();
        CyGlobalIntEnable;  // Uncomment this line to enable global interrupts. 
    
	    
		if(rs485_CheckPacket(0x55, &pak)==0) {    
            if(CheckValid(&pak)==1) {
                aileron_buffer = pak._pak.wData[0];
            }
            else {}
        } 
        else {}
        return ;
    }
}
     
void Timer_Set(unsigned int ms)
{
	Timer_WriteCounter(ms);
	return;
}

void Timer_Delayus(unsigned int us)
{
	Timer_Set(us + 50);
	while(Timer_ReadCounter() > 50) {}

}

void rs485_Start(void)
{
	UART_Start();
	Timer_Start();
	return;
}


/*
/* rs485_CheckPacket
 * 複数アドレス対応版 ビット3-6が一致で応答
 * addr : device address (7bit unsigned)
 * pak : pointer to PACKET structure
 */
/*char rs485_CheckPacket(
	char addr,
	PACKET *pak)
{
	char rcv;
	unsigned char i;
	
	if((UART_ReadRxStatus() & UART_RX_STS_FIFO_NOTEMPTY) == 0)
	{
		READ();
		return 1;
	}

	rcv = UART_ReadRxData();
	UART_ClearRxBuffer();
	if(addr != (rcv & 0x7f))
	{
		Timer_Set(200);
		READ();
        LED_3_Write(0);
		while(Timer_ReadCounter() > 49)
		{
			if((UART_ReadRxStatus() & UART_RX_STS_FIFO_NOTEMPTY) != 0)
			{
				Timer_Delayus(900);
				UART_ClearRxBuffer();
				return 1;
			}
		}
		UART_ClearRxBuffer();
		return 1;
	}
	
	WRITE();
	UART_PutChar(addr);
	while(!(UART_ReadTxStatus() & UART_TX_STS_COMPLETE)) {}
	UART_ClearRxBuffer();
	
	if(rcv & 0x80)
	{
		// send data to master
		PACKET_ENDIANCHANGE(pak);
		for(i = 0; i < 6; i++)
		{
			UART_PutChar(pak->_byte[i]);
			while(!(UART_ReadTxStatus() & UART_TX_STS_COMPLETE)) {}
			UART_ClearRxBuffer();
		}
	}
	else
	{
		// receive data from master
		READ();
		Timer_Set(1010);
		i = 0;
		while((i < 6) && (Timer_ReadCounter() > 10))
		{
			if(UART_ReadRxStatus() & UART_RX_STS_FIFO_NOTEMPTY)
			{
                LED_3_Write(0);
				pak->_byte[i++] = UART_ReadRxData();
				UART_ClearRxBuffer();
			}
		}
		//PACKET_ENDIANCHANGE(pak);
	}
	WRITE();
	UART_PutChar(addr);
	while(!(UART_ReadTxStatus() & UART_TX_STS_COMPLETE)) {}
	UART_ClearRxBuffer();
	READ();
	return 0;
}

char CheckValid(PACKET *pak)
{
    unsigned char sum = 0;
    int i;
    
    for(i = 0; i < 4; i++){
        sum += pak->_byte[i+2];    
    }
    if((~sum & 0xff) == pak->_pak.bCheckSum) {
        return 1;
    } else {
        return 0;
    }
}

*/

//void Servo_Start(void)
//{
  //  PWM_2_Start();
  //  aileron_buffer = 1500;
  //  isr_tc_StartEx(Terminal_Count_ISR);
  //  return;
//}

//CY_ISR(Terminal_Count_ISR)
//{
//    PWM_2_WriteCompare(aileron_buffer);
//    PWM_2_ReadStatusRegister();
//    return;

//}


