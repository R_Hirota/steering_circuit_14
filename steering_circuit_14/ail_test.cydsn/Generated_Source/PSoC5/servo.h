/*******************************************************************************
* File Name: servo.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_servo_H) /* Pins servo_H */
#define CY_PINS_servo_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "servo_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_90 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 servo__PORT == 15 && ((servo__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    servo_Write(uint8 value) ;
void    servo_SetDriveMode(uint8 mode) ;
uint8   servo_ReadDataReg(void) ;
uint8   servo_Read(void) ;
uint8   servo_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define servo_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define servo_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define servo_DM_RES_UP          PIN_DM_RES_UP
#define servo_DM_RES_DWN         PIN_DM_RES_DWN
#define servo_DM_OD_LO           PIN_DM_OD_LO
#define servo_DM_OD_HI           PIN_DM_OD_HI
#define servo_DM_STRONG          PIN_DM_STRONG
#define servo_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define servo_MASK               servo__MASK
#define servo_SHIFT              servo__SHIFT
#define servo_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define servo_PS                     (* (reg8 *) servo__PS)
/* Data Register */
#define servo_DR                     (* (reg8 *) servo__DR)
/* Port Number */
#define servo_PRT_NUM                (* (reg8 *) servo__PRT) 
/* Connect to Analog Globals */                                                  
#define servo_AG                     (* (reg8 *) servo__AG)                       
/* Analog MUX bux enable */
#define servo_AMUX                   (* (reg8 *) servo__AMUX) 
/* Bidirectional Enable */                                                        
#define servo_BIE                    (* (reg8 *) servo__BIE)
/* Bit-mask for Aliased Register Access */
#define servo_BIT_MASK               (* (reg8 *) servo__BIT_MASK)
/* Bypass Enable */
#define servo_BYP                    (* (reg8 *) servo__BYP)
/* Port wide control signals */                                                   
#define servo_CTL                    (* (reg8 *) servo__CTL)
/* Drive Modes */
#define servo_DM0                    (* (reg8 *) servo__DM0) 
#define servo_DM1                    (* (reg8 *) servo__DM1)
#define servo_DM2                    (* (reg8 *) servo__DM2) 
/* Input Buffer Disable Override */
#define servo_INP_DIS                (* (reg8 *) servo__INP_DIS)
/* LCD Common or Segment Drive */
#define servo_LCD_COM_SEG            (* (reg8 *) servo__LCD_COM_SEG)
/* Enable Segment LCD */
#define servo_LCD_EN                 (* (reg8 *) servo__LCD_EN)
/* Slew Rate Control */
#define servo_SLW                    (* (reg8 *) servo__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define servo_PRTDSI__CAPS_SEL       (* (reg8 *) servo__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define servo_PRTDSI__DBL_SYNC_IN    (* (reg8 *) servo__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define servo_PRTDSI__OE_SEL0        (* (reg8 *) servo__PRTDSI__OE_SEL0) 
#define servo_PRTDSI__OE_SEL1        (* (reg8 *) servo__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define servo_PRTDSI__OUT_SEL0       (* (reg8 *) servo__PRTDSI__OUT_SEL0) 
#define servo_PRTDSI__OUT_SEL1       (* (reg8 *) servo__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define servo_PRTDSI__SYNC_OUT       (* (reg8 *) servo__PRTDSI__SYNC_OUT) 


#if defined(servo__INTSTAT)  /* Interrupt Registers */

    #define servo_INTSTAT                (* (reg8 *) servo__INTSTAT)
    #define servo_SNAP                   (* (reg8 *) servo__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_servo_H */


/* [] END OF FILE */
