/*******************************************************************************
* File Name: ail.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_ail_H) /* Pins ail_H */
#define CY_PINS_ail_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "ail_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_90 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 ail__PORT == 15 && ((ail__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    ail_Write(uint8 value) ;
void    ail_SetDriveMode(uint8 mode) ;
uint8   ail_ReadDataReg(void) ;
uint8   ail_Read(void) ;
uint8   ail_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define ail_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define ail_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define ail_DM_RES_UP          PIN_DM_RES_UP
#define ail_DM_RES_DWN         PIN_DM_RES_DWN
#define ail_DM_OD_LO           PIN_DM_OD_LO
#define ail_DM_OD_HI           PIN_DM_OD_HI
#define ail_DM_STRONG          PIN_DM_STRONG
#define ail_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define ail_MASK               ail__MASK
#define ail_SHIFT              ail__SHIFT
#define ail_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define ail_PS                     (* (reg8 *) ail__PS)
/* Data Register */
#define ail_DR                     (* (reg8 *) ail__DR)
/* Port Number */
#define ail_PRT_NUM                (* (reg8 *) ail__PRT) 
/* Connect to Analog Globals */                                                  
#define ail_AG                     (* (reg8 *) ail__AG)                       
/* Analog MUX bux enable */
#define ail_AMUX                   (* (reg8 *) ail__AMUX) 
/* Bidirectional Enable */                                                        
#define ail_BIE                    (* (reg8 *) ail__BIE)
/* Bit-mask for Aliased Register Access */
#define ail_BIT_MASK               (* (reg8 *) ail__BIT_MASK)
/* Bypass Enable */
#define ail_BYP                    (* (reg8 *) ail__BYP)
/* Port wide control signals */                                                   
#define ail_CTL                    (* (reg8 *) ail__CTL)
/* Drive Modes */
#define ail_DM0                    (* (reg8 *) ail__DM0) 
#define ail_DM1                    (* (reg8 *) ail__DM1)
#define ail_DM2                    (* (reg8 *) ail__DM2) 
/* Input Buffer Disable Override */
#define ail_INP_DIS                (* (reg8 *) ail__INP_DIS)
/* LCD Common or Segment Drive */
#define ail_LCD_COM_SEG            (* (reg8 *) ail__LCD_COM_SEG)
/* Enable Segment LCD */
#define ail_LCD_EN                 (* (reg8 *) ail__LCD_EN)
/* Slew Rate Control */
#define ail_SLW                    (* (reg8 *) ail__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define ail_PRTDSI__CAPS_SEL       (* (reg8 *) ail__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define ail_PRTDSI__DBL_SYNC_IN    (* (reg8 *) ail__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define ail_PRTDSI__OE_SEL0        (* (reg8 *) ail__PRTDSI__OE_SEL0) 
#define ail_PRTDSI__OE_SEL1        (* (reg8 *) ail__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define ail_PRTDSI__OUT_SEL0       (* (reg8 *) ail__PRTDSI__OUT_SEL0) 
#define ail_PRTDSI__OUT_SEL1       (* (reg8 *) ail__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define ail_PRTDSI__SYNC_OUT       (* (reg8 *) ail__PRTDSI__SYNC_OUT) 


#if defined(ail__INTSTAT)  /* Interrupt Registers */

    #define ail_INTSTAT                (* (reg8 *) ail__INTSTAT)
    #define ail_SNAP                   (* (reg8 *) ail__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_ail_H */


/* [] END OF FILE */
