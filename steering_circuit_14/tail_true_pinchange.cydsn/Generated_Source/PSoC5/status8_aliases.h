/*******************************************************************************
* File Name: status8.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_status8_ALIASES_H) /* Pins status8_ALIASES_H */
#define CY_PINS_status8_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"



/***************************************
*              Constants        
***************************************/
#define status8_0		status8__0__PC

#endif /* End Pins status8_ALIASES_H */

/* [] END OF FILE */
