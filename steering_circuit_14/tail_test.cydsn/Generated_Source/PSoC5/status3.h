/*******************************************************************************
* File Name: status3.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_status3_H) /* Pins status3_H */
#define CY_PINS_status3_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "status3_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_90 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 status3__PORT == 15 && ((status3__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    status3_Write(uint8 value) ;
void    status3_SetDriveMode(uint8 mode) ;
uint8   status3_ReadDataReg(void) ;
uint8   status3_Read(void) ;
uint8   status3_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define status3_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define status3_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define status3_DM_RES_UP          PIN_DM_RES_UP
#define status3_DM_RES_DWN         PIN_DM_RES_DWN
#define status3_DM_OD_LO           PIN_DM_OD_LO
#define status3_DM_OD_HI           PIN_DM_OD_HI
#define status3_DM_STRONG          PIN_DM_STRONG
#define status3_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define status3_MASK               status3__MASK
#define status3_SHIFT              status3__SHIFT
#define status3_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define status3_PS                     (* (reg8 *) status3__PS)
/* Data Register */
#define status3_DR                     (* (reg8 *) status3__DR)
/* Port Number */
#define status3_PRT_NUM                (* (reg8 *) status3__PRT) 
/* Connect to Analog Globals */                                                  
#define status3_AG                     (* (reg8 *) status3__AG)                       
/* Analog MUX bux enable */
#define status3_AMUX                   (* (reg8 *) status3__AMUX) 
/* Bidirectional Enable */                                                        
#define status3_BIE                    (* (reg8 *) status3__BIE)
/* Bit-mask for Aliased Register Access */
#define status3_BIT_MASK               (* (reg8 *) status3__BIT_MASK)
/* Bypass Enable */
#define status3_BYP                    (* (reg8 *) status3__BYP)
/* Port wide control signals */                                                   
#define status3_CTL                    (* (reg8 *) status3__CTL)
/* Drive Modes */
#define status3_DM0                    (* (reg8 *) status3__DM0) 
#define status3_DM1                    (* (reg8 *) status3__DM1)
#define status3_DM2                    (* (reg8 *) status3__DM2) 
/* Input Buffer Disable Override */
#define status3_INP_DIS                (* (reg8 *) status3__INP_DIS)
/* LCD Common or Segment Drive */
#define status3_LCD_COM_SEG            (* (reg8 *) status3__LCD_COM_SEG)
/* Enable Segment LCD */
#define status3_LCD_EN                 (* (reg8 *) status3__LCD_EN)
/* Slew Rate Control */
#define status3_SLW                    (* (reg8 *) status3__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define status3_PRTDSI__CAPS_SEL       (* (reg8 *) status3__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define status3_PRTDSI__DBL_SYNC_IN    (* (reg8 *) status3__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define status3_PRTDSI__OE_SEL0        (* (reg8 *) status3__PRTDSI__OE_SEL0) 
#define status3_PRTDSI__OE_SEL1        (* (reg8 *) status3__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define status3_PRTDSI__OUT_SEL0       (* (reg8 *) status3__PRTDSI__OUT_SEL0) 
#define status3_PRTDSI__OUT_SEL1       (* (reg8 *) status3__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define status3_PRTDSI__SYNC_OUT       (* (reg8 *) status3__PRTDSI__SYNC_OUT) 


#if defined(status3__INTSTAT)  /* Interrupt Registers */

    #define status3_INTSTAT                (* (reg8 *) status3__INTSTAT)
    #define status3_SNAP                   (* (reg8 *) status3__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_status3_H */


/* [] END OF FILE */
