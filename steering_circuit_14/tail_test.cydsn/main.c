/* ========================================
 * servo check for tail
 *
 * R.Hirota 2014/4/18
 *　
 * ========================================
*/

#include <project.h>

uint8 returnval1,returnval2;
int NEUTRAL_ELE = 1500,
    NEUTRAL_LAD = 1500,
    DIAL = 100;

int main()
{
    PWM_1_Start();
    PWM_2_Start();
    LED_1_Write(0);
    
    for(;;){

        returnval1 = Status_Reg_1_Read();
        returnval1 = ~returnval1 & 0x0f;

        switch(returnval1){ //elevator
         case 0x00:
            PWM_1_WriteCompare(NEUTRAL_ELE - DIAL*8);
            break;
         case 0x01:
            PWM_1_WriteCompare(NEUTRAL_ELE - DIAL*7);
            break;
         case 0x02:
            PWM_1_WriteCompare(NEUTRAL_ELE - DIAL*6);
            break;
         case 0x03:
            PWM_1_WriteCompare(NEUTRAL_ELE - DIAL*5);
            break;
         case 0x04:
            PWM_1_WriteCompare(NEUTRAL_ELE - DIAL*4);
            break;
         case 0x05:
            PWM_1_WriteCompare(NEUTRAL_ELE - DIAL*3);
            break;
         case 0x06:
            PWM_1_WriteCompare(NEUTRAL_ELE - DIAL*2);
            break;
         case 0x07:
            PWM_1_WriteCompare(NEUTRAL_ELE - DIAL);
            break;
         case 0x08:
            PWM_1_WriteCompare(NEUTRAL_ELE);
            break;
         case 0x09:
            PWM_1_WriteCompare(NEUTRAL_ELE + DIAL);
            break;
         case 0x0a:
            PWM_1_WriteCompare(NEUTRAL_ELE + DIAL*2);
            break;
         case 0x0b:
            PWM_1_WriteCompare(NEUTRAL_ELE + DIAL*3);
            break;
         case 0x0c:
            PWM_1_WriteCompare(NEUTRAL_ELE + DIAL*4);
            break;
         case 0x0d:
            PWM_1_WriteCompare(NEUTRAL_ELE + DIAL*5);
            break;
         case 0x0e:
            PWM_1_WriteCompare(NEUTRAL_ELE + DIAL*6);
            break;
         case 0x0f:
            PWM_1_WriteCompare(NEUTRAL_ELE + DIAL*7);
            break;
        }
        
        returnval2 = Status_Reg_2_Read();
        returnval2 = ~returnval2&0x0f;

        switch(returnval2){ //ladder
         case 0x00:
            PWM_2_WriteCompare(NEUTRAL_LAD - DIAL*8);
            break;
         case 0x01:
            PWM_2_WriteCompare(NEUTRAL_LAD - DIAL*7);
            break;
         case 0x02:
            PWM_2_WriteCompare(NEUTRAL_LAD - DIAL*6);
            break;
         case 0x03:
            PWM_2_WriteCompare(NEUTRAL_LAD - DIAL*5);
            break;
         case 0x04:
            PWM_2_WriteCompare(NEUTRAL_LAD - DIAL*4);
            break;
         case 0x05:
            PWM_2_WriteCompare(NEUTRAL_LAD - DIAL*3);
            break;
         case 0x06:
            PWM_2_WriteCompare(NEUTRAL_LAD - DIAL*2);
            break;
         case 0x07:
            PWM_2_WriteCompare(NEUTRAL_LAD - DIAL);
            break;
         case 0x08:
            PWM_2_WriteCompare(NEUTRAL_LAD);
            break;
         case 0x09:
            PWM_2_WriteCompare(NEUTRAL_LAD + DIAL);
            break;
         case 0x0a:
            PWM_2_WriteCompare(NEUTRAL_LAD + DIAL*2);
            break;
         case 0x0b:
            PWM_2_WriteCompare(NEUTRAL_LAD + DIAL*3);
            break;
         case 0x0c:
            PWM_2_WriteCompare(NEUTRAL_LAD + DIAL*4);
            break;
         case 0x0d:
            PWM_2_WriteCompare(NEUTRAL_LAD + DIAL*5);
            break;
         case 0x0e:
            PWM_2_WriteCompare(NEUTRAL_LAD + DIAL*6);
            break;
         case 0x0f:
            PWM_2_WriteCompare(NEUTRAL_LAD + DIAL*7);
            break;
        }
    }
}

/* [] END OF FILE */
