/*******************************************************************************
* File Name: DE.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_DE_ALIASES_H) /* Pins DE_ALIASES_H */
#define CY_PINS_DE_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"



/***************************************
*              Constants        
***************************************/
#define DE_0		DE__0__PC

#endif /* End Pins DE_ALIASES_H */

/* [] END OF FILE */
