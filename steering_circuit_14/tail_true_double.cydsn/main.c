/* ========================================
 * tail14 double 2014/6/5
 * 
 * R.Hirota
 * ========================================
*/
#include <project.h>

void WRITE(){
    if(Control_Reg_1_Read()){
        DE_0_Write(0),DE_1_Write(1);
    }
    else{
        DE_0_Write(1),DE_1_Write(0);
    }
}

void READ(){
    DE_0_Write(0),DE_1_Write(0);
}

#define	RS485_PACKET_LENGTH	(6)

typedef union _tagRS485_PACKET {
	struct {
		unsigned char bStatus;
		unsigned char bCheckSum;
		unsigned short wData[2];
	} _pak;
	unsigned char _byte[RS485_PACKET_LENGTH];
} PACKET;

void rs485_Start(void);
void rs485_isr(void);
char rs485_CheckPacket(char, PACKET*);
char CheckValid(PACKET *);
void Servo_Start(void);
void Terminal_Count_ISR(void);
unsigned int FormatValueElv(unsigned short);
unsigned int FormatValueRad(unsigned short);
int CenterStatus1(void);
int CenterStatus2(void);
void SignalChange(void);

#define	PACKET_ENDIANCHANGE(x)	{\
	char temp; \
	temp = x->_byte[2]; \
	x->_byte[2] = x->_byte[3]; \
	x->_byte[3] = temp; \
	temp = x->_byte[4]; \
	x->_byte[4] = x->_byte[5]; \
	x->_byte[5] = temp; \
	}

//Global variable here.

unsigned int
	elevator_buffer, 
	radder_buffer;
    rad_now;
    rad_error;
PACKET pak;

int counter = 0;

void main(){ 
    
    rs485_Start();   
    Servo_Start();
    CyGlobalIntEnable;  // Uncomment this line to enable global interrupts. 

	while(1) {
		if(rs485_CheckPacket(0x55, &pak)==0) {
            LED_2_Write(1);
            LED_1_Write(0);
            LED_4_Write(1);
            if(CheckValid(&pak)==1) {
                counter++;
                elevator_buffer = pak._pak.wData[0];
				radder_buffer = pak._pak.wData[1];
                LED_2_Write(0);
                LED_1_Write(0);
                LED_4_Write(1);
            } else {
                LED_1_Write(1);
                LED_2_Write(1);
                LED_4_Write(1);
            }
        } else {
            LED_2_Write(1);
            LED_4_Write(1);
            if(counter > -500){
                counter--;
            }else{
                SignalChange();
                LED_4_Write(0);
                counter = 0;
            }
        } 
	}
    return;
}

void Timer_Set(unsigned int ms){
	Timer_WriteCounter(ms);
	return;
}

void Timer_Delayus(unsigned int us){
	Timer_Set(us + 50);
	while(Timer_ReadCounter() > 50) {}
	return;
}

void rs485_Start(void){
	UART_Start();
	Timer_Start();
	return;
}

/*
 * rs485_CheckPacket
 * 複数アドレス対応版 ビット3-6が一致で応答
 * addr : device address (7bit unsigned)
 * pak : pointer to PACKET structure
 */
char rs485_CheckPacket(
	char addr,
	PACKET *pak)
{
	char rcv;
	unsigned char i;
	
	if((UART_ReadRxStatus() & UART_RX_STS_FIFO_NOTEMPTY) == 0)
	{
        LED_1_Write(1);
		READ();
		return 1;
	}
	LED_1_Write(1);
	rcv = UART_ReadRxData();
	UART_ClearRxBuffer();
	if(addr != (rcv & 0x7f)){
		Timer_Set(200);
		READ();
		while(Timer_ReadCounter() > 49){
			if((UART_ReadRxStatus() & UART_RX_STS_FIFO_NOTEMPTY) != 0){
				Timer_Delayus(900);
				UART_ClearRxBuffer();
				return 1;
			}
		}
		UART_ClearRxBuffer();
		return 1;
	}
	
    LED_1_Write(0);
	WRITE();
	UART_PutChar(addr);
	while(!(UART_ReadTxStatus() & UART_TX_STS_COMPLETE)) {}
	UART_ClearRxBuffer();
	
	if(rcv & 0x80){
		// send data to master
		PACKET_ENDIANCHANGE(pak);
		for(i = 0; i < 6; i++){
			UART_PutChar(pak->_byte[i]);
			while(!(UART_ReadTxStatus() & UART_TX_STS_COMPLETE)) {}
			UART_ClearRxBuffer();
		}
	}
	else{
		// receive data from master
		READ();
		Timer_Set(1010);
		i = 0;
		while((i < 6) && (Timer_ReadCounter() > 10)){
			if(UART_ReadRxStatus() & UART_RX_STS_FIFO_NOTEMPTY){
				pak->_byte[i++] = UART_ReadRxData();
				UART_ClearRxBuffer();
			}
		}
		//PACKET_ENDIANCHANGE(pak);
	}
	WRITE();
	UART_PutChar(addr);
	while(!(UART_ReadTxStatus() & UART_TX_STS_COMPLETE)) {}
	UART_ClearRxBuffer();
    READ();
	return 0;
}

char CheckValid(PACKET *pak){
    unsigned char sum = 0;
    int i;
    
    for(i = 0; i < 4; i++){
        sum += pak->_byte[i+2];    
    }
    if((~sum & 0xff) == pak->_pak.bCheckSum) {
        return 1;
    } else {
        return 0;
    }
}

int CenterStatus1(void){
	int Trim1 ,dTrim1 = 25;
	int sw1 = (~ Status_Reg_1_Read()) & 0x0F ;
	switch(sw1){
		case 0x00 :
			Trim1 = -8*dTrim1;
			break;	
		case 0x01 :
			Trim1 = -7*dTrim1;
			break;
		case 0x02 :
			Trim1 = -6*dTrim1;
			break;
		case 0x03 :
			Trim1 = -5*dTrim1;
			break;
		case 0x04 :
			Trim1 = -4*dTrim1;
			break;
		case 0x05 :
			Trim1 = -3*dTrim1;
			break;
		case 0x06 :
			Trim1 = -2*dTrim1;
			break;
		case 0x07 :
			Trim1 = -1*dTrim1;
			break;
		case 0x08 :
			Trim1 = 0*dTrim1;
			break;
		case 0x09 :
			Trim1 = 1*dTrim1;
			break;
		case 0x0A :
			Trim1 = 2*dTrim1;
			break;
		case 0x0B :
			Trim1 = 3*dTrim1;
			break;
		case 0x0C :
			Trim1 = 4*dTrim1;
			break;
		case 0x0D :
			Trim1 = 5*dTrim1;
			break;
		case 0x0E :
			Trim1 = 6*dTrim1;
			break;
		case 0x0F :
			Trim1 = 7*dTrim1;
			break;
	}
	return(Trim1);
}

int CenterStatus2(void){
	int Trim2 ,dTrim2 = 10;
	int sw2 = (~ Status_Reg_2_Read()) & 0x0F ;
	switch(sw2){
		case 0x00 :
			Trim2 = -8*dTrim2;
			break;	
		case 0x01 :
			Trim2 = -7*dTrim2;
			break;
		case 0x02 :
			Trim2 = -6*dTrim2;
			break;
		case 0x03 :
			Trim2 = -5*dTrim2;
			break;
		case 0x04 :
			Trim2 = -4*dTrim2;
			break;
		case 0x05 :
			Trim2 = -3*dTrim2;
			break;
		case 0x06 :
			Trim2 = -2*dTrim2;
			break;
		case 0x07 :
			Trim2 = -1*dTrim2;
			break;
		case 0x08 :
			Trim2 = 0*dTrim2;
			break;
		case 0x09 :
			Trim2 = 1*dTrim2;
			break;
		case 0x0A :
			Trim2 = 2*dTrim2;
			break;
		case 0x0B :
			Trim2 = 3*dTrim2;
			break;
		case 0x0C :
			Trim2 = 4*dTrim2;
			break;
		case 0x0D :
			Trim2 = 5*dTrim2;
			break;
		case 0x0E :
			Trim2 = 6*dTrim2;
			break;
		case 0x0F :
			Trim2 = 7*dTrim2;
			break;
	}
	return(Trim2);
}

void Servo_Start(void){
	elevator_buffer = 1500;
	radder_buffer = 1500;
	PWM_1_Start();
    isr_tc_StartEx(Terminal_Count_ISR);
    Control_Reg_1_Write(0);
    return;
}

unsigned int FormatValueElv(unsigned short raw_elv){
	raw_elv = 1400 + (int)CenterStatus1() - (raw_elv - 1500)*1.3;
	if((raw_elv <= 2200)&&(raw_elv >= 800)){
		return raw_elv;
	}else{
		return PWM_1_ReadCompare1();
	}
}

unsigned int FormatValueRad(unsigned short raw_rad){
	raw_rad += (int)CenterStatus2();
    rad_now = PWM_1_ReadCompare2();
    if((((raw_rad <= 2200) && (raw_rad >= 800) && (raw_rad < rad_now + 500) && (raw_rad > rad_now - 500)))||(rad_error > 5)){
        return raw_rad;
        rad_error = 0;
    }
    else{
        return rad_now;
        rad_error++;
    }
}

CY_ISR(Terminal_Count_ISR) {
	PWM_1_WriteCompare1(FormatValueElv(elevator_buffer));
	PWM_1_WriteCompare2(FormatValueRad(radder_buffer));
    PWM_1_ReadStatusRegister();
    return;
}

void SignalChange(void){
    if(Control_Reg_1_Read()){
        Control_Reg_1_Write(0);
    }
    else{
        Control_Reg_1_Write(1);
    }   
}
/* [] END OF FILE */

