/*******************************************************************************
* File Name: rud.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_rud_H) /* Pins rud_H */
#define CY_PINS_rud_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "rud_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_90 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 rud__PORT == 15 && ((rud__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    rud_Write(uint8 value) ;
void    rud_SetDriveMode(uint8 mode) ;
uint8   rud_ReadDataReg(void) ;
uint8   rud_Read(void) ;
uint8   rud_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define rud_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define rud_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define rud_DM_RES_UP          PIN_DM_RES_UP
#define rud_DM_RES_DWN         PIN_DM_RES_DWN
#define rud_DM_OD_LO           PIN_DM_OD_LO
#define rud_DM_OD_HI           PIN_DM_OD_HI
#define rud_DM_STRONG          PIN_DM_STRONG
#define rud_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define rud_MASK               rud__MASK
#define rud_SHIFT              rud__SHIFT
#define rud_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define rud_PS                     (* (reg8 *) rud__PS)
/* Data Register */
#define rud_DR                     (* (reg8 *) rud__DR)
/* Port Number */
#define rud_PRT_NUM                (* (reg8 *) rud__PRT) 
/* Connect to Analog Globals */                                                  
#define rud_AG                     (* (reg8 *) rud__AG)                       
/* Analog MUX bux enable */
#define rud_AMUX                   (* (reg8 *) rud__AMUX) 
/* Bidirectional Enable */                                                        
#define rud_BIE                    (* (reg8 *) rud__BIE)
/* Bit-mask for Aliased Register Access */
#define rud_BIT_MASK               (* (reg8 *) rud__BIT_MASK)
/* Bypass Enable */
#define rud_BYP                    (* (reg8 *) rud__BYP)
/* Port wide control signals */                                                   
#define rud_CTL                    (* (reg8 *) rud__CTL)
/* Drive Modes */
#define rud_DM0                    (* (reg8 *) rud__DM0) 
#define rud_DM1                    (* (reg8 *) rud__DM1)
#define rud_DM2                    (* (reg8 *) rud__DM2) 
/* Input Buffer Disable Override */
#define rud_INP_DIS                (* (reg8 *) rud__INP_DIS)
/* LCD Common or Segment Drive */
#define rud_LCD_COM_SEG            (* (reg8 *) rud__LCD_COM_SEG)
/* Enable Segment LCD */
#define rud_LCD_EN                 (* (reg8 *) rud__LCD_EN)
/* Slew Rate Control */
#define rud_SLW                    (* (reg8 *) rud__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define rud_PRTDSI__CAPS_SEL       (* (reg8 *) rud__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define rud_PRTDSI__DBL_SYNC_IN    (* (reg8 *) rud__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define rud_PRTDSI__OE_SEL0        (* (reg8 *) rud__PRTDSI__OE_SEL0) 
#define rud_PRTDSI__OE_SEL1        (* (reg8 *) rud__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define rud_PRTDSI__OUT_SEL0       (* (reg8 *) rud__PRTDSI__OUT_SEL0) 
#define rud_PRTDSI__OUT_SEL1       (* (reg8 *) rud__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define rud_PRTDSI__SYNC_OUT       (* (reg8 *) rud__PRTDSI__SYNC_OUT) 


#if defined(rud__INTSTAT)  /* Interrupt Registers */

    #define rud_INTSTAT                (* (reg8 *) rud__INTSTAT)
    #define rud_SNAP                   (* (reg8 *) rud__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_rud_H */


/* [] END OF FILE */
