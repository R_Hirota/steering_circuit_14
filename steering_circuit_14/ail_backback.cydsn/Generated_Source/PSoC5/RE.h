/*******************************************************************************
* File Name: RE.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_RE_H) /* Pins RE_H */
#define CY_PINS_RE_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "RE_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_90 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 RE__PORT == 15 && ((RE__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    RE_Write(uint8 value) ;
void    RE_SetDriveMode(uint8 mode) ;
uint8   RE_ReadDataReg(void) ;
uint8   RE_Read(void) ;
uint8   RE_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define RE_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define RE_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define RE_DM_RES_UP          PIN_DM_RES_UP
#define RE_DM_RES_DWN         PIN_DM_RES_DWN
#define RE_DM_OD_LO           PIN_DM_OD_LO
#define RE_DM_OD_HI           PIN_DM_OD_HI
#define RE_DM_STRONG          PIN_DM_STRONG
#define RE_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define RE_MASK               RE__MASK
#define RE_SHIFT              RE__SHIFT
#define RE_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define RE_PS                     (* (reg8 *) RE__PS)
/* Data Register */
#define RE_DR                     (* (reg8 *) RE__DR)
/* Port Number */
#define RE_PRT_NUM                (* (reg8 *) RE__PRT) 
/* Connect to Analog Globals */                                                  
#define RE_AG                     (* (reg8 *) RE__AG)                       
/* Analog MUX bux enable */
#define RE_AMUX                   (* (reg8 *) RE__AMUX) 
/* Bidirectional Enable */                                                        
#define RE_BIE                    (* (reg8 *) RE__BIE)
/* Bit-mask for Aliased Register Access */
#define RE_BIT_MASK               (* (reg8 *) RE__BIT_MASK)
/* Bypass Enable */
#define RE_BYP                    (* (reg8 *) RE__BYP)
/* Port wide control signals */                                                   
#define RE_CTL                    (* (reg8 *) RE__CTL)
/* Drive Modes */
#define RE_DM0                    (* (reg8 *) RE__DM0) 
#define RE_DM1                    (* (reg8 *) RE__DM1)
#define RE_DM2                    (* (reg8 *) RE__DM2) 
/* Input Buffer Disable Override */
#define RE_INP_DIS                (* (reg8 *) RE__INP_DIS)
/* LCD Common or Segment Drive */
#define RE_LCD_COM_SEG            (* (reg8 *) RE__LCD_COM_SEG)
/* Enable Segment LCD */
#define RE_LCD_EN                 (* (reg8 *) RE__LCD_EN)
/* Slew Rate Control */
#define RE_SLW                    (* (reg8 *) RE__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define RE_PRTDSI__CAPS_SEL       (* (reg8 *) RE__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define RE_PRTDSI__DBL_SYNC_IN    (* (reg8 *) RE__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define RE_PRTDSI__OE_SEL0        (* (reg8 *) RE__PRTDSI__OE_SEL0) 
#define RE_PRTDSI__OE_SEL1        (* (reg8 *) RE__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define RE_PRTDSI__OUT_SEL0       (* (reg8 *) RE__PRTDSI__OUT_SEL0) 
#define RE_PRTDSI__OUT_SEL1       (* (reg8 *) RE__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define RE_PRTDSI__SYNC_OUT       (* (reg8 *) RE__PRTDSI__SYNC_OUT) 


#if defined(RE__INTSTAT)  /* Interrupt Registers */

    #define RE_INTSTAT                (* (reg8 *) RE__INTSTAT)
    #define RE_SNAP                   (* (reg8 *) RE__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_RE_H */


/* [] END OF FILE */
