/*******************************************************************************
* File Name: status8.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_status8_H) /* Pins status8_H */
#define CY_PINS_status8_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "status8_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_90 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 status8__PORT == 15 && ((status8__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    status8_Write(uint8 value) ;
void    status8_SetDriveMode(uint8 mode) ;
uint8   status8_ReadDataReg(void) ;
uint8   status8_Read(void) ;
uint8   status8_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define status8_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define status8_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define status8_DM_RES_UP          PIN_DM_RES_UP
#define status8_DM_RES_DWN         PIN_DM_RES_DWN
#define status8_DM_OD_LO           PIN_DM_OD_LO
#define status8_DM_OD_HI           PIN_DM_OD_HI
#define status8_DM_STRONG          PIN_DM_STRONG
#define status8_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define status8_MASK               status8__MASK
#define status8_SHIFT              status8__SHIFT
#define status8_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define status8_PS                     (* (reg8 *) status8__PS)
/* Data Register */
#define status8_DR                     (* (reg8 *) status8__DR)
/* Port Number */
#define status8_PRT_NUM                (* (reg8 *) status8__PRT) 
/* Connect to Analog Globals */                                                  
#define status8_AG                     (* (reg8 *) status8__AG)                       
/* Analog MUX bux enable */
#define status8_AMUX                   (* (reg8 *) status8__AMUX) 
/* Bidirectional Enable */                                                        
#define status8_BIE                    (* (reg8 *) status8__BIE)
/* Bit-mask for Aliased Register Access */
#define status8_BIT_MASK               (* (reg8 *) status8__BIT_MASK)
/* Bypass Enable */
#define status8_BYP                    (* (reg8 *) status8__BYP)
/* Port wide control signals */                                                   
#define status8_CTL                    (* (reg8 *) status8__CTL)
/* Drive Modes */
#define status8_DM0                    (* (reg8 *) status8__DM0) 
#define status8_DM1                    (* (reg8 *) status8__DM1)
#define status8_DM2                    (* (reg8 *) status8__DM2) 
/* Input Buffer Disable Override */
#define status8_INP_DIS                (* (reg8 *) status8__INP_DIS)
/* LCD Common or Segment Drive */
#define status8_LCD_COM_SEG            (* (reg8 *) status8__LCD_COM_SEG)
/* Enable Segment LCD */
#define status8_LCD_EN                 (* (reg8 *) status8__LCD_EN)
/* Slew Rate Control */
#define status8_SLW                    (* (reg8 *) status8__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define status8_PRTDSI__CAPS_SEL       (* (reg8 *) status8__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define status8_PRTDSI__DBL_SYNC_IN    (* (reg8 *) status8__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define status8_PRTDSI__OE_SEL0        (* (reg8 *) status8__PRTDSI__OE_SEL0) 
#define status8_PRTDSI__OE_SEL1        (* (reg8 *) status8__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define status8_PRTDSI__OUT_SEL0       (* (reg8 *) status8__PRTDSI__OUT_SEL0) 
#define status8_PRTDSI__OUT_SEL1       (* (reg8 *) status8__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define status8_PRTDSI__SYNC_OUT       (* (reg8 *) status8__PRTDSI__SYNC_OUT) 


#if defined(status8__INTSTAT)  /* Interrupt Registers */

    #define status8_INTSTAT                (* (reg8 *) status8__INTSTAT)
    #define status8_SNAP                   (* (reg8 *) status8__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_status8_H */


/* [] END OF FILE */
