/*******************************************************************************
* File Name: ele.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_ele_H) /* Pins ele_H */
#define CY_PINS_ele_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "ele_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_90 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 ele__PORT == 15 && ((ele__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    ele_Write(uint8 value) ;
void    ele_SetDriveMode(uint8 mode) ;
uint8   ele_ReadDataReg(void) ;
uint8   ele_Read(void) ;
uint8   ele_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define ele_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define ele_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define ele_DM_RES_UP          PIN_DM_RES_UP
#define ele_DM_RES_DWN         PIN_DM_RES_DWN
#define ele_DM_OD_LO           PIN_DM_OD_LO
#define ele_DM_OD_HI           PIN_DM_OD_HI
#define ele_DM_STRONG          PIN_DM_STRONG
#define ele_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define ele_MASK               ele__MASK
#define ele_SHIFT              ele__SHIFT
#define ele_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define ele_PS                     (* (reg8 *) ele__PS)
/* Data Register */
#define ele_DR                     (* (reg8 *) ele__DR)
/* Port Number */
#define ele_PRT_NUM                (* (reg8 *) ele__PRT) 
/* Connect to Analog Globals */                                                  
#define ele_AG                     (* (reg8 *) ele__AG)                       
/* Analog MUX bux enable */
#define ele_AMUX                   (* (reg8 *) ele__AMUX) 
/* Bidirectional Enable */                                                        
#define ele_BIE                    (* (reg8 *) ele__BIE)
/* Bit-mask for Aliased Register Access */
#define ele_BIT_MASK               (* (reg8 *) ele__BIT_MASK)
/* Bypass Enable */
#define ele_BYP                    (* (reg8 *) ele__BYP)
/* Port wide control signals */                                                   
#define ele_CTL                    (* (reg8 *) ele__CTL)
/* Drive Modes */
#define ele_DM0                    (* (reg8 *) ele__DM0) 
#define ele_DM1                    (* (reg8 *) ele__DM1)
#define ele_DM2                    (* (reg8 *) ele__DM2) 
/* Input Buffer Disable Override */
#define ele_INP_DIS                (* (reg8 *) ele__INP_DIS)
/* LCD Common or Segment Drive */
#define ele_LCD_COM_SEG            (* (reg8 *) ele__LCD_COM_SEG)
/* Enable Segment LCD */
#define ele_LCD_EN                 (* (reg8 *) ele__LCD_EN)
/* Slew Rate Control */
#define ele_SLW                    (* (reg8 *) ele__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define ele_PRTDSI__CAPS_SEL       (* (reg8 *) ele__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define ele_PRTDSI__DBL_SYNC_IN    (* (reg8 *) ele__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define ele_PRTDSI__OE_SEL0        (* (reg8 *) ele__PRTDSI__OE_SEL0) 
#define ele_PRTDSI__OE_SEL1        (* (reg8 *) ele__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define ele_PRTDSI__OUT_SEL0       (* (reg8 *) ele__PRTDSI__OUT_SEL0) 
#define ele_PRTDSI__OUT_SEL1       (* (reg8 *) ele__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define ele_PRTDSI__SYNC_OUT       (* (reg8 *) ele__PRTDSI__SYNC_OUT) 


#if defined(ele__INTSTAT)  /* Interrupt Registers */

    #define ele_INTSTAT                (* (reg8 *) ele__INTSTAT)
    #define ele_SNAP                   (* (reg8 *) ele__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_ele_H */


/* [] END OF FILE */
